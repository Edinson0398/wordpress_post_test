---
ID: 2946
post_author: "4"
post_date: 2018-11-03 04:57:25
post_date_gmt: 2018-11-03 04:57:25
post_title: CaseCard
post_excerpt: CaseCard
post_status: inherit
comment_status: open
ping_status: closed
post_password: ""
post_name: casecard-3
to_ping: ""
pinged: ""
post_modified: 2018-11-03 04:57:34
post_modified_gmt: 2018-11-03 04:57:34
post_content_filtered: ""
post_parent: 1654
guid: >
  http://localhost/wordpress/wp-content/uploads/2018/09/afb921a1-casecard.png
menu_order: 0
post_type: attachment
post_mime_type: image/png
comment_count: "0"
filter: raw
---
The fourth and last card of a particular rank to become available.