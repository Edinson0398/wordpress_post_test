---
ID: 2601
post_author: "2"
post_date: 2018-10-05 16:07:43
post_date_gmt: 2018-10-05 16:07:43
post_title: Coinflip
post_excerpt: CoinFlip
post_status: inherit
comment_status: open
ping_status: closed
post_password: ""
post_name: coinflip
to_ping: ""
pinged: ""
post_modified: 2018-10-05 16:09:40
post_modified_gmt: 2018-10-05 16:09:40
post_content_filtered: ""
post_parent: 1658
guid: >
  http://localhost/wordpress/wp-content/uploads/2018/09/1d4eb876-coinflip.png
menu_order: 0
post_type: attachment
post_mime_type: image/png
comment_count: "0"
filter: raw
---
A coinflip (coin toss) refers to a situation in which two players have the same chance of winning. As it is very close to being 50:50, the hand could also decide to flip a coin.