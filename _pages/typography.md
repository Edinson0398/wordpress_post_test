---
ID: 186
post_author: "7"
post_date: 2017-10-17 12:55:43
post_date_gmt: 2017-10-17 12:55:43
post_title: Typography
post_excerpt: ""
post_status: publish
comment_status: closed
ping_status: closed
post_password: ""
post_name: typography
to_ping: ""
pinged: ""
post_modified: 2017-10-17 12:55:43
post_modified_gmt: 2017-10-17 12:55:43
post_content_filtered: ""
post_parent: 0
guid: >
  http://pokav2.pokatheme.com/demo1/?page_id=186
menu_order: 0
post_type: page
post_mime_type: ""
comment_count: "0"
filter: raw
---
<strong>Below you can find examples of typography.</strong>
<h1>Header title H1</h1>
<h2>Header title H2</h2>
<h3>Header title H3</h3>
<h4>Header title H4</h4>
<h5>Header title H5</h5>
<h6>Header title H6</h6>

<hr />

<h2>Paragraphs</h2>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. <em>Donec non lobortis nulla</em>. Vivamus ornare lacus et arcu pulvinar vestibulum. Praesent id lobortis velit. Aenean eu magna id elit sagittis pharetra non in dui. Morbi convallis, sapien at sollicitudin cursus, erat nibh imperdiet erat, nec tempor est sem vitae eros. Maecenas eu finibus nulla, ac venenatis urna. Maecenas sit amet finibus metus. Aenean rhoncus velit sit amet lorem interdum aliquam. <strong>Mauris facilisis porta erat aliquet porttitor</strong>. Quisque non sem massa. Morbi egestas ligula ac sem suscipit dictum. Etiam in lacus rutrum dui rutrum condimentum non in ipsum. Ut ultrices laoreet elit, sit amet vehicula ante imperdiet ut. Praesent at aliquam tellus. Donec ullamcorper, diam ut iaculis iaculis, turpis nunc lacinia ex, at suscipit nisi massa vitae nibh. Mauris purus libero, <i>consectetur sed quam at</i>, ullamcorper elementum justo.

Duis eget velit non sem mattis viverra sed commodo libero. <a href="http://www.google.com">Etiam dolor dolor</a>, maximus vitae turpis sodales, egestas vestibulum massa. <a href="#">Vivamus lorem neque</a>, vehicula eu quam in, consectetur semper nulla. Nam et scelerisque neque, vel convallis mauris. Ut id luctus ipsum, sit amet euismod lectus. Vivamus ornare felis vitae elit feugiat, ac efficitur lacus venenatis. Sed nisl metus, auctor eget tortor sit amet, blandit placerat tellus. Proin nec aliquet augue. Mauris ornare purus et purus maximus suscipit. Phasellus eget porta mauris, nec consectetur nibh. Phasellus tempus consequat arcu sit amet congue. Donec non blandit lorem, ut mollis diam. Nulla feugiat turpis et ligula eleifend fermentum. Praesent lacinia elementum felis, nec sollicitudin quam mollis in.

Phasellus pellentesque, augue id rhoncus condimentum, turpis dolor facilisis odio, eget gravida metus nibh sed lectus. Ut sed lectus purus. Integer mattis rhoncus lectus at pretium. Phasellus finibus nec lectus vel egestas. Etiam sit amet ex id neque euismod aliquet. Duis luctus tortor orci, vel facilisis est vulputate a. Pellentesque ultricies est ut purus hendrerit, id scelerisque enim bibendum.

Nam ut lobortis sem. Vestibulum sodales pellentesque tortor, non fermentum orci cursus sit amet. Praesent feugiat dictum augue id porta. Quisque tortor urna, tristique nec ipsum at, lacinia rutrum nulla. Nulla convallis vestibulum cursus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean blandit ex ipsum, blandit dignissim odio auctor eget. Maecenas vehicula, massa eget porta aliquet, mauris diam tempor enim, <small>vitae blandit odio elit a nunc. Nam eu pretium eros</small>.

<hr />

<h2>Lists</h2>
<ul>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
<ol>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ol>
[two-cols-first]
<ul class="text-list text-list--one">
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
[/two-cols-first]
[two-cols-last]
<ul class="text-list text-list--two">
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
[/two-cols-last]
[two-cols-first]
<ul class="text-list text-list--three">
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
[/two-cols-first]
[two-cols-last]
<ul class="text-list text-list--four">
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
[/two-cols-last]
[two-cols-first]
<ul class="text-list text-list--five">
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
[/two-cols-first]
[two-cols-last]
<ul class="text-list text-list--six">
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
 	<li>Vivamus commodo diam augue</li>
 	<li>unc suscipit suscipit turpis at fermentum</li>
</ul>
[/two-cols-last]

<hr />

<h3>Tables</h3>
<table><caption>This is an example table, and this is its caption to describe the contents.</caption>
<thead>
<tr>
<th>Table heading</th>
<th>Table heading</th>
<th>Table heading</th>
<th>Table heading</th>
</tr>
</thead>
<tbody>
<tr>
<td>Table cell</td>
<td>Table cell</td>
<td>Table cell</td>
<td>Table cell</td>
</tr>
<tr>
<td>Table cell</td>
<td>Table cell</td>
<td>Table cell</td>
<td>Table cell</td>
</tr>
<tr>
<td>Table cell</td>
<td>Table cell</td>
<td>Table cell</td>
<td>Table cell</td>
</tr>
</tbody>
</table>

<hr />

<h3>Sample form</h3>
[contact-form-7 id="198" title="Sample form"]

<hr />

<h3>Address</h3>
<address><strong>Twitter, Inc.</strong>
1355 Market St, Suite 900
San Francisco, CA 94103
<abbr title="Phone">P:</abbr> (123) 456-7890</address><address><strong>Blockquote</strong>
<a href="mailto:#">first.last@example.com</a></address>

<hr />

<h3>Blockquote</h3>
<blockquote class="blockquote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.

<footer>Someone famous in <cite title="Source Title">Source Title</cite></footer></blockquote>