---
ID: 155
post_author: "7"
post_date: 2017-10-17 12:10:08
post_date_gmt: 2017-10-17 12:10:08
post_title: General shortcodes
post_excerpt: ""
post_status: publish
comment_status: closed
ping_status: closed
post_password: ""
post_name: general-shortcodes
to_ping: ""
pinged: ""
post_modified: 2017-10-17 12:10:08
post_modified_gmt: 2017-10-17 12:10:08
post_content_filtered: ""
post_parent: 0
guid: >
  http://pokav2.pokatheme.com/demo1/?page_id=155
menu_order: 0
post_type: page
post_mime_type: ""
comment_count: "0"
filter: raw
---
[box_text]In this page you can find all the available shortcodes for general purposes. You can use them in any page/post you want and customize them with each shortcode properties.[/box_text]
<h3>Slideshow</h3>
You can create unlimited slideshows in <strong>Sliders CPT</strong> and then display the slideshow you want with this shortcode.

[poka_slider title="Slider1"]

&nbsp;
<h3>Posts shortcode</h3>
With this shortcode you can display a certain number of posts from a selected category (or without selecting a category).

[latest_news]

&nbsp;
<h3>Posts shortcode / style 2</h3>
Another style for displaying posts inside a page.

[news_boxes]

&nbsp;
<h3>Posts shortcode / style 3</h3>
This style for displaying posts is made for limited spaces like a narrow column or inside a widget!

[latest_news_sidebar]

&nbsp;
<h3>Text boxes with FontAwesome icons</h3>
If you want to make some text to pop out you can use this shortcode and even select an icon from huge FontAwesome icons gallery.

[box_text icon=""]Nam sollicitudin nunc eu ex facilisis faucibus. Proin sem sem, sollicitudin vitae maximus non, ornare id nisi. Vivamus pellentesque quam mauris, nec blandit quam fringilla ac. Sed posuere sodales odio. Sed rutrum eleifend ipsum id accumsan. Morbi eget facilisis leo. Praesent ac risus hendrerit, egestas enim ut, efficitur lectus. Praesent lacinia finibus felis in volutpat. Maecenas faucibus arcu at turpis efficitur volutpat. Morbi rutrum fringilla rhoncus. Quisque faucibus vitae sapien id porttitor. Sed nec libero ligula. Praesent convallis lectus imperdiet leo porttitor, vel luctus mi condimentum. Integer vel tellus nec mauris facilisis semper a non tortor. [/box_text]

&nbsp;

[box_text icon=""] In mattis varius ipsum non iaculis. Sed maximus augue ut turpis mollis, vitae feugiat ante ullamcorper. Donec vel ante consectetur, dapibus libero non, volutpat metus. Vestibulum posuere commodo maximus. Morbi vestibulum risus et lorem condimentum iaculis. Morbi varius, diam eget iaculis tincidunt, ante odio fermentum augue, mattis vehicula risus dui in arcu. Aenean eget porta velit. Mauris sagittis eu ligula vitae maximus. Phasellus ut arcu id mauris rhoncus lobortis. Nunc cursus tortor et cursus rutrum. Aliquam erat volutpat.

Nam sollicitudin nunc eu ex facilisis faucibus. Proin sem sem, sollicitudin vitae maximus non, ornare id nisi. Vivamus pellentesque quam mauris, nec blandit quam fringilla ac. Sed posuere sodales odio. Sed rutrum eleifend ipsum id accumsan. Morbi eget facilisis leo. Praesent ac risus hendrerit, egestas enim ut, efficitur lectus. Praesent lacinia finibus felis in volutpat. Maecenas faucibus arcu at turpis efficitur volutpat. Morbi rutrum fringilla rhoncus. Quisque faucibus vitae sapien id porttitor. Sed nec libero ligula. Praesent convallis lectus imperdiet leo porttitor, vel luctus mi condimentum. Integer vel tellus nec mauris facilisis semper a non tortor. [/box_text]

&nbsp;
<h3>Text box</h3>
[box_text]Nam sollicitudin nunc eu ex facilisis faucibus. Proin sem sem, sollicitudin vitae maximus non, ornare id nisi. Vivamus pellentesque quam mauris, nec blandit quam fringilla ac. Sed posuere sodales odio. Sed rutrum eleifend ipsum id accumsan. Morbi eget facilisis leo. Praesent ac risus hendrerit, egestas enim ut, efficitur lectus. [/box_text]

&nbsp;
<h3>Social links</h3>
When you set your social profiles in theme settings you can use this shortcode in order to display them.

[social_links]