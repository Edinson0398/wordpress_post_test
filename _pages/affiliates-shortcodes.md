---
ID: 183
post_author: "7"
post_date: 2017-10-17 12:55:12
post_date_gmt: 2017-10-17 12:55:12
post_title: Affiliates shortcodes
post_excerpt: ""
post_status: publish
comment_status: closed
ping_status: closed
post_password: ""
post_name: affiliates-shortcodes
to_ping: ""
pinged: ""
post_modified: 2017-10-17 12:55:12
post_modified_gmt: 2017-10-17 12:55:12
post_content_filtered: ""
post_parent: 0
guid: >
  http://pokav2.pokatheme.com/demo1/?page_id=183
menu_order: 0
post_type: page
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h3>Affiliates ajax search</h3>
You can place this search form wherever you want and the visitors can search your reviews with autocomplete and find their favorite almost immediately!

[search_affiliates_ajax]

&nbsp;
<h3>Affiliate free spins single view</h3>
A single affiliate view for your selected review showing the free spins bonus. (In this example it is a two columns layout)

[two-cols-first][single_affiliate_freespins title="Guts"][/two-cols-first] [two-cols-last][single_affiliate_freespins title="Bet365"][/two-cols-last]

&nbsp;
<h3>Affiliate single view</h3>
A single affiliate view for your selected review showing main bonus. (In this example it is a two columns layout)

[two-cols-first][single_affiliate title="888Poker"][/two-cols-first] [two-cols-last][single_affiliate title="bgoVegas"][/two-cols-last]

&nbsp;
<h3>Affiliate single view / large</h3>
A single affiliate view for your selected review in a large format.

[single_affiliate title="betsafe" size="big"]

&nbsp;
<h3>Affiliates list / two columns</h3>
You can select the number of reviews you want to display, select a category if you want and display a list of reviews.

[affiliates_list num="4" sort="date" columns="2"]

&nbsp;