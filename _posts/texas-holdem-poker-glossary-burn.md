---
ID: 1646
post_author: "1"
post_date: 2018-09-07 13:44:47
post_date_gmt: 2018-09-07 13:44:47
post_title: 'Texas Holdem Poker Glossary: Burn'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-burn
to_ping: ""
pinged: ""
post_modified: 2018-11-06 20:35:15
post_modified_gmt: 2018-11-06 20:35:15
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1646
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2><b>Whats is Burn?</b></h2>
<b>Burn</b> or <b>Burn Card</b>, in Texas Holdem Poker, is a security measure casinos do to avoid card marking. It's usually the first card of the deck.

The dealer must burn the top card of the deck after every betting round, right before dealing the card. This is just to make sure that none of the active players counted or marked any card and second dealing. That burned cars goes shown to the players and goes straight to the muck.

The muck is nothing but the discarded cards of the deals of the table.

[adinserter name="Block 14"]

&nbsp;

<b>Poker Rules - Poker Tutorials - Video -</b>

This is a very interesting tutorial video to see. Because even though is not  a poker game, it looks like one. Nicky Numbers shows us a very helpful video to understand more the basics of poker. He acts like the dealer of the casino. In minute 1:30 he starts to deal cards and we can appreciate that he doesn’t turn the top card of the deck. That card is our burned card.

&nbsp;

https://www.youtube.com/watch?v=oLSMasFvzxE

&nbsp;

[adinserter name="Block 2"]
<h3><b>Terrible Mistakes by Poker Pros and Dealers - Video -</b></h3>
To make a little more interesting and interactive this article, we decided to post this video. This next video is a compilation of various poker mistakes made by dealers and professional players. In the first video of the compilation we can see a very unfortunate and very rare mistake: the burnt of a player's card. A mistake made by both the dealer and the player. As for the second video if the compilation, we can appreciate a good dealing. In fact, in that video the mistake was made by the player who burnt the winning cards. Don't miss the other mistakes!

https://www.youtube.com/watch?v=9ioAyMtZHIc

[adinserter name="Block 1"]
<h3>Craziest Poker Hand Ever! What a Burn!!! - Video -</h3>
Observar este video realmente es genial, genial por el resultado y genial porque la victoria de ROBL es ABSOLUTA, a tal punto de que se dispone sin pausa a apostar todas las cartas que tiene a su favor. durante la partida todos están muy serenos y serios, pero cuando ROBL gana, es increíble la forma en la que todos quedan fuera de lugar. Hay un minuto que nos llama mucho la atención que es el minuto 1:03, Nos gusta mucho el gesto de Timoshenko en este momento cuando realiza una de sus apuestas en esta partida, es verdaderamente interesante como maneja la situación con tanta confianza y desenvolvimiento.

https://www.youtube.com/watch?v=d3Wrli2oxF0

&nbsp;

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 15"]