---
ID: 1658
post_author: "8"
post_date: 2018-09-07 14:02:32
post_date_gmt: 2018-09-07 14:02:32
post_title: 'Texas Holdem Poker Glossary: Coin Flips'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-coin-flips
to_ping: ""
pinged: ""
post_modified: 2018-10-05 16:11:28
post_modified_gmt: 2018-10-05 16:11:28
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1658
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h1>Coinflip</h1>
Un <i><a class="glosslink" href="https://es.pokerstrategy.com/glossary/Coinflip/">coinflip</a></i> (lanzamiento de una moneda) se refiere a una situación en la que dos jugadores tienen prácticamente las mismas posibilidades de ganar. Como está muy cerca de ser 50:50, la mano podría decidirse también lanzando una moneda.

&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]