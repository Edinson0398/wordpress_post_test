---
ID: 1652
post_author: "1"
post_date: 2018-09-07 13:45:42
post_date_gmt: 2018-09-07 13:45:42
post_title: 'Texas Holdem Poker Glossary: Calling Station'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: >
  texas-holdem-poker-glossary-calling-station
to_ping: ""
pinged: ""
post_modified: 2018-11-09 18:28:30
post_modified_gmt: 2018-11-09 18:28:30
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1652
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is a Calling Station?</h2>
<strong>A Calling Station</strong> or Loose-Passive player is one of the <a href="http://localhost/wordpress/four-types-of-poker-players/">four type of of poker player</a>. They are famous for being very easy and obvious targets at the table; yet very hard to bluff. This nickname is a pejorative adjective to describe his/her way of playing which is loose and passive.

When we say loose we mean that he/she doesn't have a real strategy when it comes to selecting the hands. As for passive we mean that he/she avoids confrontation and he/she is fearful of losing money. Which is why always calls and rarely raises.

This type of player usually call <em>just to see a flop</em> and if they hit any card, they won't fold.

&nbsp;

[adinserter name="Block 1"]
<h3>Sky Poker Strategy - Player Types: Calling Station - Video -</h3>
Interesante esta entrevista a un jugador de poker bastante recorrido y dedicado al tema, quien nos da una excelente explicación sobre Calling station y algunos otros temas en el poker. Siempre es bueno escuchar otros puntos de vista, a veces nuestra manera de ver las cosas va de la mano con la persona que oimos, y a veces sencillamente reforzamos la manera diferente en que nosotros actuamos. De cualquier manera Escucha esto con atención y evalua tu opinión sobre estos comentarios.

https://www.youtube.com/watch?v=4ZiQTt8WvbA

[adinserter name="Block 2"]

&nbsp;
<h3>Playing Against Calling Stations - Sky Poker School - Video -</h3>
De una manera diferente nos muestran en este video una partida de poker, no es con personas directamente y no es referente a la manera original de jugar poker, pero tal vez de forma virtual nos sirva mucho para aprender mejor ciertos términos y así manejarnos mejor en la mesa. siempre la idea principal es esa, movilizarnos hacia la búsqueda de mejorar, y mientras hayan otras alternativas de estudiar esto, consideramos es mucho mejor para nosotros.

https://www.youtube.com/watch?v=oERs_8OgtNI

[adinserter name="Block 3"]

&nbsp;
<h3>Viffer tries to bluff a Calling station - Video -</h3>
&nbsp;

https://www.youtube.com/watch?v=SNYWmj0ggmA

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]

&nbsp;