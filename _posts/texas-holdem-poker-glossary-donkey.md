---
ID: 1675
post_author: "1"
post_date: 2018-09-07 14:15:39
post_date_gmt: 2018-09-07 14:15:39
post_title: 'Texas Holdem Poker Glossary: Donkey'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-donkey
to_ping: ""
pinged: ""
post_modified: 2018-11-03 02:22:01
post_modified_gmt: 2018-11-03 02:22:01
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1675
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>Donkey</h2>
<div class="sectionIntro">

A derogatory term used to refer to a weak, unskilled player. Abbreviated “donk.”

</div>
&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]