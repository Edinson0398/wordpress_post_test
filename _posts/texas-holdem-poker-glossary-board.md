---
ID: 1635
post_author: "1"
post_date: 2018-09-07 13:43:33
post_date_gmt: 2018-09-07 13:43:33
post_title: 'Texas Holdem Poker Glossary: Board'
post_excerpt: >
  Board is slang or the informal way to
  call the community cards.
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-board
to_ping: ""
pinged: ""
post_modified: 2018-11-07 17:58:43
post_modified_gmt: 2018-11-07 17:58:43
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1635
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is Board?</h2>
Board, in poker, are nothing but the five <a href="http://localhost/wordpress/best-texas-holdem-poker-hands/">community cards</a> per deal. There are many things regarding the board, but one of the most important are: the different textures of it; and/or if it's dynamic or static.
<h2>Textures of the Board</h2>
Texture is a reference on how connected the community cards are. Such connection allows the players to see the potential hands they can create. Textures are mostly taken into consideration during the flop, since that's one of the most important rounds to create your hand. There are usually two textures: <strong>wet (wet board)</strong> or <strong>dry</strong> <strong>(dry</strong> <strong>board)</strong>.
<h3>Wet Board</h3>
Wet is one of the possible textures of the board. This texture is the good one. The one that will help your hand. Now, a Wet Board is a reference to the community cards: they are either suited or connected. Connected in a way that will allow flushes or straights.

Let's exemplify this:
<ol>
 	<li>A flop with Aces, Kings and Two's can be considered as wet. Since it can help to create flushes or straights.</li>
 	<li>A flop with seven's, eight's and nine's can be considered as wet. As for this cards can help to create flushes or straights.</li>
</ol>
<h3>Dry Board</h3>
Dry

[adinserter name="Block 1"]

&nbsp;

<img class="alignnone wp-image-2480" src="http://localhost/wordpress/wp-content/uploads/2018/09/f4b79c4f-loser-1024x683-300x200.jpg" alt="" width="524" height="349" />

&nbsp;

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]