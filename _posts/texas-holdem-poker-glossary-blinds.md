---
ID: 1633
post_author: "1"
post_date: 2018-09-07 13:43:16
post_date_gmt: 2018-09-07 13:43:16
post_title: 'Texas Holdem Poker Glossary: Blinds'
post_excerpt: 'Blinds are forced bets made by the two players to the immediate left of the dealer (or the player that has the dealer button). Usually there are two blinds: the small blind and the big blind'
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-blinds
to_ping: ""
pinged: ""
post_modified: 2018-11-07 17:46:34
post_modified_gmt: 2018-11-07 17:46:34
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1633
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What are Blinds?</h2>
Blinds, in Texas Holdem Poker, are compulsory bets made at the beginning of the deal by the two players to the immediate left of the <a href="http://localhost/wordpress/texas-holdem-poker-glossary-dealer/">dealer.</a>

That said, the only exception to this rule is when there are only two players- or <a href="http://localhost/wordpress/texas-holdem-pok…lossary-heads-up/">heads up</a>-. In this case, the dealer is the small blind and the other player the big blind.

The number of blinds can vary depending on the size of the table. It can go from none to three.Nevertheless, usually there are two blinds: the <strong>small blind</strong> and the <strong>big blind</strong>. The size of these bets vary depending on the variation of the game your playing.

For example, in Limit Texas Holdem, the small blind is the half of the lower limit. While the big blind is the lower limit.

It's worth saying that since the blinds are always made by the two players next to the dealer the dealer figure chances on each deal.
<h4>Small Blind</h4>
The small blind is the first bet of the deal. Since it's made by the player to the immediate left of the dealer it's the smallest bet allowed.

<img class="wp-image-2489 aligncenter" alt="" width="563" height="375" />

&nbsp;

&nbsp;
<h4>Big Blind</h4>
The big blind is made by the player next to the small blind. And it becomes the minimum bet of the deal, until another player raises the bet.

It's important to mention that after the blinds are made, the players designated as blinds are given another chance to bet. Since raise and re-raises might happen and they have to either call or raise the current bet. Especially the small blind, this player must call the initial bet.
<h4><img class="wp-image-2488 aligncenter" src="http://localhost/wordpress/wp-content/uploads/2018/09/5e060513-the-blinds-poker-300x225.jpg" alt="" width="543" height="407" /></h4>
&nbsp;
<h3>Poker Blinds - Poker Tutorials - Video -</h3>
Nicky Numbers is here with his expertise to help us understand more the blinds. This is a very easy educative-interactive video. With examples on the table while explaining the definition.

https://www.youtube.com/watch?v=IdtLI-lSrVY

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

&nbsp;