---
ID: 1661
post_author: "8"
post_date: 2018-09-07 14:13:31
post_date_gmt: 2018-09-07 14:13:31
post_title: 'Texas Holdem Poker Glossary: Connectors'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-connectors
to_ping: ""
pinged: ""
post_modified: 2018-10-05 16:30:01
post_modified_gmt: 2018-10-05 16:30:01
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1661
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2 class="glossarySection--content-headline">Conectores</h2>
Son cartas consecutivas como 78, 45, 23. Si son del mismo palo, entonces se denominan <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Conectores/">conectores</a> del mismo palo (<i><a class="glosslink" href="https://es.pokerstrategy.com/glossary/Suited_117/">suited</a>connectors</i>); en caso contrario, son <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Conectores/">conectores</a> de distinto palo (<i>unsuited connectors</i>). También hay <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Conectores/">conectores</a> con un hueco (<i>one-gap</i>) como 35 ó 68, <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Conectores/">conectores</a> con dos huecos (<i>two-gap</i>) como 25 y 96.

Son manos interesantes porque es fácil formar escaleras con ellas. Cuanto más cerca estén las cartas, más probabilidades de lograr la <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Escalera/">escalera</a>. Es fácil de <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Ver/">ver</a> si se listan todas las escaleras que se pueden hacer con 78 comparadas con las de 69.

<u>Connectors – Cartas conectadas</u>
Dos cartas de mano de valores consecutivos, como 8-9 o 4-5. Son deseables porque tienen una mayor probabilidad de formar escalera.

&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]