---
ID: 1785
post_author: "1"
post_date: 2018-09-07 15:20:22
post_date_gmt: 2018-09-07 15:20:22
post_title: 'Texas Holdem Poker Glossary: Tilt'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-tilt
to_ping: ""
pinged: ""
post_modified: 2018-11-07 18:15:24
post_modified_gmt: 2018-11-07 18:15:24
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1785
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h1>Tilt</h1>
This is a negative psychological reaction to an outcome, most likely a bad beat. Whenever a player is not playing optimally and making poor decision, they are on <a class="definitionMatch" href="https://en.clubpoker.net/tilt/definition-567" data-ipstooltip="">tilt</a>.

Un jugador entra en <em>tilt</em> cuando pierde el control debido a que ha sufrido un Bad Beat, o una mala racha, o a que otro jugador lo ha insultado, etc. En estado de <em>tilt</em> el jugador empeora su juego y aumenta así sus probabilidades de perder una gran suma.

&nbsp;

&nbsp;

&nbsp;

[adinserter name="Block 1"]
<h3 class="title style-scope ytd-video-primary-info-renderer">Tilting in Poker | Poker Tutorials - Video -</h3>
This is a very interesting video Tutorial about the Tilting, this men, always say things very nice, he have some videos of poker, the diferents things in poker in youtube, if you pay atention, you can to understand very very well aboout the tilt. The words used fotr he, was very easy for to undernstand and to go to play poker and to haven't eny problem with tilt.

https://www.youtube.com/watch?v=1BiJZjMUU6Q

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]
<p style="padding-left: 30px;">[adinserter name="Block 13"]</p>
[adinserter name="Block 14"]

[adinserter name="Block 15"]