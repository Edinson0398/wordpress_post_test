---
ID: 1618
post_author: "1"
post_date: 2018-09-07 13:35:55
post_date_gmt: 2018-09-07 13:35:55
post_title: 'Texas Holdem Poker Glossary: Ante'
post_excerpt: 'An Antes is a forced bet made by all players at the beginning of the deal. Antes are different from Blinds. Even so, most of time whenever one is used, the other is not.  '
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-ante
to_ping: ""
pinged: ""
post_modified: 2018-11-07 15:37:06
post_modified_gmt: 2018-11-07 15:37:06
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1618
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is an Ante?</h2>
An Ante is a compulsory small bet that is used to start the pot before the initial deal begins. The name of this bet comes from the Latin word <em>ante</em> and means before. Which might explain why the bet is done before the deal.

Antes bets are different from the <a href="http://localhost/wordpress/texas-holdem-poker-glossary-blinds/">Blinds</a>. In one hand, it must be done by all players in the table before the hand starts. In another hand the blinds are only made by the two players to the left of the button by deal. Also, this bet is much smaller than the regular minimum or blind bet.

In a 10$/20$ Texas Holdem Poker game, the Ante might be around 2$/3$ or a chip with the smallest denomination. It's important to mention that it's very rare to use both (antes and blinds) at the same time in a poker game.

Antes are usually used in 7-Stud games. However, lately is been used in Texas Holdem Poker games in tournaments, but in the later rounds to add more interest to the pot. For example, in the WSOP the ante isn't use up until the fifth level.

[adinserter name="Block 1"]

&nbsp;

<img class="wp-image-2309 aligncenter" src="http://localhost/wordpress/wp-content/uploads/2018/09/3ae38dd8-poker-antes-300x135.jpg" alt="" width="738" height="332" />

&nbsp;
<h3>Poker Antes - Poker Tutorials - Video -</h3>
Nicky Numbers. Another professional poker player that help us to understand in an easy way the hole poker world. This is a simple but clear tutorial about the differences between antes and blinds. It's a very useful video since it exemplifies with chips the how the antes work in contrast to the blinds.

https://www.youtube.com/watch?v=MSeG1ZWVApE

&nbsp;

[adinserter name="Block 2"]

&nbsp;
<h3>Negreanu Small Ball - Blinds and Antes - Video -</h3>
Daniel Negreanu is simply the best man to explain Poker in YouTube. He always speaks from his professional and personal knowledge, and you know, it's a man with a great poker experience. The way he explains it is just amazing and on this video he will help us understand better (and easier) the differences between Antes and Blinds.

https://www.youtube.com/watch?v=4zTmOnlNVAY

&nbsp;

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]

&nbsp;

&nbsp;