---
ID: 8
post_author: "1"
post_date: 2018-08-22 13:58:44
post_date_gmt: 2018-08-22 13:58:44
post_title: History of Texas Holdem Poker
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: history-of-texas-holdem-poker
to_ping: ""
pinged: ""
post_modified: 2018-09-05 18:05:45
post_modified_gmt: 2018-09-05 18:05:45
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=8
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
The History of Texas Holdem Poker is as long as unknown. It's a centenary card game that gained popularity when the Internet came out. Replacing Draw Poker and Stud Poker and transitioning to a worldwide juggernaut in current times.
<h2>French Poque and Texas Holdem Poker</h2>
Most people agree that poker originated in France and started out as a popular game called “poque”. When the 1700's wave of migration hit the United States the poque-loving Frenchmen brought their favorite card game with them. Eventually, "poque" transformed into poker and well... the rest is history.

The history of poker claims that the earliest version of Texas Holdem was born in Robstown, Texas, in the early 1900's. The specific conditions in which the game emerged aren't exactly clear. There are some stories that say that cowboys and miners mixed gambling games and drinking, naming this card game as "Devil's River"; to this day the final round in Texas Holdem is known as river.

Now, the one certain thing is that Texas Holdem originated in Texas, where it was simply known as Holdem Poker. Then, when the game was introduced to Las Vegas in then late 1900's, the full name changed to Texas Holdem Poker.

&nbsp;

https://www.youtube.com/watch?v=bPBPepsZbFA
<h2></h2>
<h2>A Slow Start in Las Vegas</h2>
The Golden Nugger Casino brought Texas Holdem Poker to Las Vegas in 1967. Nevertheless, this place was considered a bit of a seedy joint due to its downtown location, stopping many high-stakes players from coming in to try the game. So, the game remained in relative obscurity at the beginning of its run.

In 1969, a Texas Holdem tournament was held in the lobby of The Dune Casino. Thanks to its location – right on The Strip – the tournament proved to be financially rewarding for a number of high rollers. Having tasted the fun and profit of the game, the popularity of the game increased exponentially. Players like Doyle Brunson and Amarillo Slim became some of the first well-known Texas Holdem gamblers.

It was not long until a man called Benny Binion came into the Texas Holdem Poker scene that the popularity of the game began to rise.  He was the founder of the Horseshoe Casino and was considered a man with a questionable reputation and extensive experience in high-stakes match arrangements. A perfect combination for what was about to happen.

&nbsp;

<img class="wp-image-265 aligncenter" src="http://localhost/wordpress/wp-content/uploads/2018/08/persons-300x86.jpg" alt="" width="805" height="231" />

&nbsp;

&nbsp;
<h2>The World Series of Poker</h2>
In 1970 the History of Texas Holdem Poker took a decisive turn. Mr. Binion started the first World Series Of Poker tournament inviting 6 high-rollers to take part. The event was a huge success within the public, something  unheard of. How could a card tournament be as spectacular to watch as sports matches? Yet, it was true.  That's the moment when everybody caught wind of the new favorite: Texas Holdem Poker. Those who were in the tournament told other people about the experience, and the rest is history.

Ever since 1971, the second year of the series, the main event of this tournament has been a no-limit Texas Holdem game. At first the amount of entrants was small. Over the years thousands of people compete in the main event of the World Series of Poker each year.
<h2></h2>
<h2>Poker literature<img class="size-medium wp-image-523 alignleft" src="http://localhost/wordpress//wp-content/uploads/2018/08/libros-300x225.png" alt="" width="300" height="225" /></h2>
A couple books about Texas Holdem were released between the 70's and the 80's. They contributed to increase the knowledge about this rising card game and transformed the perspective people had about it. Causing a turning point in the history of Texas Holdem Poker.

<em>Super System </em>(1978) is the name of one of these books. The author is famous poker player: Doyle Brunson. This publication is about poker strategies and changed dramatically the way people conceived and played Texas Holdem.

Another other book on the list is <em>The Biggest Game in Town </em>(1983)<em>.</em> It was written by the English poet and novelist known as Al Alvarez. It's about the World Series of Poker of 1981. This book increased the popularity of Texas Holdem around the world and brought the public's attention to the professional poker players.  These books were the starting point of the poker literature genre.
<h2>The Online World Revolution</h2>
The most revolutionary development in the history of Texas Holdem came with the creation of the online casinos in the 1990's. Later, in 2013, Chris Moneymaker won the World Series of Poker entering with only 40$. In that moment, he became the first player in the WSOF to have qualified on an online poker site. The national TV broadcasted his victory, reason why his $2,5 million dollar victory had a huge effect in the growth of poker worldwide. Moneymaker's triumph and access to online poker made of Texas Holdem the most played card game of the world.

&nbsp;

&nbsp;
<h2>Where to Play Texas Holdem Poker Online</h2>
As said before, the Internet was one of the greatest prime movers in the history of Texas Holdem. Nowadays, there are a lot of websites in which you can play Texas Holdem online. Those sites give a wide range of options to the players, choosing the bests depends on what the player wants. Some of the best rated ones in the Internet are:
<h2></h2>
<h2><img class="wp-image-291 alignleft" src="http://localhost/wordpress/wp-content/uploads/2018/08/ponline_poker_-300x240.png" alt="" width="297" height="238" /></h2>
<h2>Poker Stars</h2>
Many people think that is the best online poker site in the market. It's the largest and the most used one in the world. Poker Stars offers one of the best choices of cash games and tournaments. They created the Sunday Million, which is the biggest weekly poker tournament worldwide and attracts the best poker players. The accumulated prize is 1.000.000$. Poker Stars is compatible with Windows and Mac.

&nbsp;

&nbsp;

&nbsp;
<h2>888 Poker<img class=" wp-image-293 alignleft" src="http://localhost/wordpress/wp-content/uploads/2018/08/888Poker-Online-300x146.png" alt="" width="298" height="145" /></h2>
This website has more than 10 million of associates around the world. It's the online poker room that is growing faster, every 12 seconds a player registers. 888 Poker offers a wide variety of cash games ang online tournaments, independently of your budget or abilities. It's available for different platforms such as Windows, Mac, Linux,  Android and iOS.

&nbsp;
<h2>Party Poker<img class="size-medium wp-image-484 alignleft" src="http://localhost/wordpress/wp-content/uploads/2018/08/sinnombre___-300x188.jpg" alt="" width="300" height="188" /></h2>
Party Poker was created in 2001 and offers cash games, tournaments, fast forward poker and sit &amp; go tourneys. All of these options are available in it's website and mobile apps for Android (worldwide) and iOS (selected countries).
This website was the biggest online poker room until 2006.

&nbsp;

&nbsp;

&nbsp;
<h2>Pacific Poker<img class="size-medium wp-image-297 alignleft" src="http://localhost/wordpress/wp-content/uploads/2018/08/pacific-300x199.jpg" alt="" width="300" height="199" /></h2>
This online poker room is powered by 888 Poker and gives the players diffents options such as playing for cash, a practice-mode, sit &amp; go poker and bonuses. Pacific Poker also has mobile platforms for Android and iOS.

&nbsp;

&nbsp;