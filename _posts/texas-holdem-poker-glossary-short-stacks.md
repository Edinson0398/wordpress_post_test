---
ID: 1761
post_author: "1"
post_date: 2018-09-07 15:16:01
post_date_gmt: 2018-09-07 15:16:01
post_title: 'Texas Holdem Poker Glossary: Short Stacks'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-short-stacks
to_ping: ""
pinged: ""
post_modified: 2018-11-07 17:19:22
post_modified_gmt: 2018-11-07 17:19:22
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1761
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h3>SHORT STACKS</h3>
Un <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Stack-%28Caja%29_128/">stack (caja)</a> es corto si es pequeño comparado con el stack de los adversarios, con el máximo <a class="glosslink" href="https://es.pokerstrategy.com/glossary/buy-in/">buy-in</a>, o con el <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Ante/">ante</a>. Un jugador que tenga ese tamaño de stack también se denomina <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Shortstack/">shortstack</a>. Por ejemplo, un jugador en una <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Mesa/">mesa</a> con el máximo <a class="glosslink" href="https://es.pokerstrategy.com/glossary/buy-in/">buy-in</a> de 100 big bets y que tenga sólo 20 big bets es denominado <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Shortstack/">shortstack</a>.

Tener un número reducido de fichas conlleva un impacto <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Directo_2032/">directo</a> en el juego. Un <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Shortstack/">shortstack</a> no puede realizar movimientos que sean complicados o demasiado caros. Sólo puede ganar el mismo valor de su stack, por lo tanto, no puede jugar manos especulativas puesto que sus <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Odds-implicitas/">odds implícitas</a> son insuficientes.

En un <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Torneo/">torneo</a>, un <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Shortstack/">shortstack</a> se encuentra bajo presión para doblar o triplicar su stack lo antes posible, no puede esperar demasiado para que le llegue una mano buena, puesto que el <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Ante/">ante</a> devorará rápidamente su stack.

Too web site with interesting information about the stacks ;)

https://www.pokernews.com/strategy/keep-your-tournament-game-simple-stack-sizes-25067.htm

https://es.pokerstrategy.com/strategy/sss/que-es-short-stack-estrategia/
http://www.pokerology.com/lessons/chip-stack-management/

[adinserter name="Block 1"]
<h3>Phil Hellmuth: Short Stack Strategy - Video -</h3>
Estos dos personajes nos narran de una manera bastante entendible a qué nos referimos cuando se habla de un Short Stack, nos ayudan enormemente a entender este concepto pero a su vez nos guían para saber cómo reaccionar, qué hacer, dé que manera continuar el juego de forma acertada o positiva cuando tienes Short Stack. Entre interrogantes, dudas, anécdotas y cuentos, se va logrando una verdadera aclaración de lo que se entiende como Short Stack.

https://www.youtube.com/watch?v=_KN02BGYYYQ

[adinserter name="Block 2"]

&nbsp;
<h3>Shorstack Cash gane Poker Strategy- Video -</h3>
El video a continuación, muestra una explicación escrita sobre esta estrategia, puedes ver una lista para hacer check en aquellas cosas que vayas logrando captar o comprender. A parte nos hace aclaratoria de otros conceptos y términos importantes de tener en cuenta siempre.

https://www.youtube.com/watch?v=eKc1D4hlL2I

[adinserter name="Block 3"]
<h3></h3>
<img class="alignnone wp-image-2081" src="http://localhost/wordpress/wp-content/uploads/2018/09/3009b894-poker-chips-game-short-stack-shortstacked-300x200.jpg" alt="" width="728" height="485" />

<img class="alignnone wp-image-2292" src="http://localhost/wordpress/wp-content/uploads/2018/09/b9778df3-texas-holdem-poker-300x200.jpg" alt="" width="728" height="485" />

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]
[adinserter name="Block 14"]

[adinserter name="Block 15"]