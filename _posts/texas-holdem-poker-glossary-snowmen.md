---
ID: 1775
post_author: "1"
post_date: 2018-09-07 15:18:16
post_date_gmt: 2018-09-07 15:18:16
post_title: 'Texas Holdem Poker Glossary: Snowmen'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-snowmen
to_ping: ""
pinged: ""
post_modified: 2018-11-07 15:42:53
post_modified_gmt: 2018-11-07 15:42:53
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1775
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h3>SHOWMEN</h3>
<ol>
 	<li>A slang term for the Hold'em hand of pocket eights, otherwise known as 88, a play on words to refer how the number 8 looks like a snowman.</li>
</ol>
&nbsp;

&nbsp;

[adinserter name="Block 1"]
<h3>Snowmen - Video -</h3>
En el video podrás notar, aunque se vea un poco pixelado, cómo en la puesta de las cartas sobre la mesa, en el segundo 0:46, aparece a mano derecha en la parte superior un par de cartas con número 8, una de pica y otra de trebol, eso es un Snowmen; que incluso en el segundo 0:05, el jugador Kjell Monsen muestra sus cartas antes de comenzar la partida. Mientras que Lovelace tiene una K y una A de diamante. El doble ocho o Snowmen es una muy buena mano, pero en este video se puede notar cómo todas las cartas y la suerte juega a favor de Kjell Monsen, puesto que resulta vencedor al final, cosa que sabemos que pudo de igual forma no ser así.

https://www.youtube.com/watch?v=3lkFVg7PK60

&nbsp;

&nbsp;

[adinserter name="Block 2"]

<img class="alignnone wp-image-2208" src="http://localhost/wordpress/wp-content/uploads/2018/09/7f776ebc-snowmen-300x225.jpeg" alt="" width="867" height="651" />

&nbsp;

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

&nbsp;