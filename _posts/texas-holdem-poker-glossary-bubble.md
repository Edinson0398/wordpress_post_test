---
ID: 1642
post_author: "1"
post_date: 2028-09-07 13:44:29
post_date_gmt: 2028-09-07 13:44:29
post_title: 'Texas Holdem Poker Glossary: Bubble'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-bubble
to_ping: ""
pinged: ""
post_modified: 2018-11-26 19:50:24
post_modified_gmt: 2018-11-26 19:50:24
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1642
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2 class="entry-title">What is Bubble?</h2>
Bubble, in Texas Holdem Poker, can have many meanings and uses. However, the most important meaning or its base meaning is "the place or time before all the prizes are awarded".

Let's exemplify it. We are in a 200 person tournament, but only 20 will be awarded. The bubble would be the moment when there are only 30 players left fighting to get to the top 15.
<h2>Bubble-boy or Bubbled</h2>
Bubble-boy is the last player to be eliminated before the prizes are given, during the bubble period. You could also say or hear "to have bubbled the event" to refer to the unfortunate persona that didn't make the cut.

For instance, we are in a tournament that only gives prizes to the top 5 poker players. The 6th place is the bubbled person of the tournament.
<h3></h3>
<h3>Bubble Play - Video -</h3>
Presta un poco de atención a Daniel Negreanu sobre lo que tiene para decirnos en este video sobre Bubble, su recorrido y conocimiento sin duda nos permite entender mejor de qué trata este items en el mundo del poker. Sin duda toda una experiencia de vida la de Daniel en este juego, así que no dejes de escucharlo.

https://www.youtube.com/watch?v=U5bXE0McJh0

&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]