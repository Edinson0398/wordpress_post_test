---
ID: 1640
post_author: "1"
post_date: 2018-09-07 13:44:17
post_date_gmt: 2018-09-07 13:44:17
post_title: 'Texas Holdem Poker Glossary: Broadway'
post_excerpt: 'Brodway is the informal way or slang for a Straight Hand. This hand got its name due to the "big actors" of the legendary New York  theaters. If you have a straight you have the big cards (10 to ace) unsuited. '
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-broadway
to_ping: ""
pinged: ""
post_modified: 2018-11-26 15:30:22
post_modified_gmt: 2018-11-26 15:30:22
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1640
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is a Broadway?</h2>
Broadway is slang for a Straight hand. Most hands have an official name and an informal way or slang. In this case Broadway and Main Street are nicknames for an Ace High Straight. This hand though it's a good hand it's not the best since there are flushes, full houses, fours of a kind, straights flushes and royal flushes out there. This term refers to the New York theaters of the era when Texas Holdem was starting to get famous. If you had a Broadways Straigh in a play, you had all the big actors. In Poker if you have a Broadway, you have all the big cards (from 10 to ace) unsuited.

[adinserter name="Block 1"]
<h3>When BOTH players play the board! - Video -</h3>
What are the chances that the community cards are also a Straight hand? Not many, but in this case our board consists of one. Both players decide to play the board until they realize is hand itself. After the river turn they both play their hole two paired with community cards to not share the pot.

https://www.youtube.com/watch?v=b3HAm4f-5_U

&nbsp;

<img class="alignnone wp-image-2471" src="http://localhost/wordpress/wp-content/uploads/2018/09/f1bab74f-brydz-300x225.jpg" alt="" width="587" height="440" />

[adinserter name="Block 2"]

&nbsp;

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]