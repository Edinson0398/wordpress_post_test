---
ID: 1672
post_author: "1"
post_date: 2018-09-07 14:15:23
post_date_gmt: 2018-09-07 14:15:23
post_title: 'Texas Holdem Poker Glossary: Dolly Parton'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-dolly-parton
to_ping: ""
pinged: ""
post_modified: 2018-11-12 15:57:05
post_modified_gmt: 2018-11-12 15:57:05
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1672
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<div class="sectionIntro">
<h2>Dolly Parton</h2>
A straight 5-9 (Nine to five). Named after her starring role in the film <i>9 to 5</i>.

</div>
<ol>
 	<li>The pocket cards 9-5, either suited or offsuit.ADDITIONAL INFORMATION: This hand name originates from the fact that celebrity songwriter Dolly Parton wrote the song "Nine to Five" and starred in a movie sharing the same name.
<div>SAMPLE HAND:</div>
<img src="http://dictionary.pokerzone.com/i/cards_lg/9s.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/5d.gif" width="32" height="46" />EXAMPLE: "I had Dolly Parton in the pocket on the small blind and decided to stay in and see a flop."

APPLIES TO: Online and Land-based Venues

GAME CATEGORY: Shared Card Games

SYNONYMS: <a href="http://dictionary.pokerzone.com/Office+Hours">Office Hours</a>, <a href="http://dictionary.pokerzone.com/Worker's+Hand">Worker's Hand</a></li>
 	<li><em>Noun</em>A nine-high straight, the straight 9-8-7-6-5.
<div>SAMPLE HAND:</div>
<img src="http://dictionary.pokerzone.com/i/cards_lg/9d.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/8s.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/7d.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/6c.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/5s.gif" width="32" height="46" />EXAMPLE: "I had a suited oldsmobile on the pocket and flopped Dolly Parton."

APPLIES TO: Online and Land-based Venues

SYNONYMS: <a href="http://dictionary.pokerzone.com/Office+Hours">Office Hours</a>, <a href="http://dictionary.pokerzone.com/Worker's+Hand">Worker's Hand</a>

RELATED TERMS: <a href="http://dictionary.pokerzone.com/Straight">Straight</a></li>
 	<li></li>
</ol>
[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]