---
ID: 1671
post_author: "1"
post_date: 2018-09-07 14:15:07
post_date_gmt: 2018-09-07 14:15:07
post_title: 'Texas Holdem Poker Glossary: Deuce'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-deuce
to_ping: ""
pinged: ""
post_modified: 2018-11-03 02:22:53
post_modified_gmt: 2018-11-03 02:22:53
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1671
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>Deuce</h2>
A card with the rank two; the 2s, 2c, 2h, or 2d.
<div>SAMPLE CARDS:</div>
<img src="http://dictionary.pokerzone.com/i/cards_lg/2s.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/2c.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/2h.gif" width="32" height="46" /> <img src="http://dictionary.pokerzone.com/i/cards_lg/2d.gif" width="32" height="46" />

EXAMPLE: "I needed an eight to complete my straight, but the river was a deuce."

APPLIES TO: Online and Land-based Venues
<ol>
 	<li>The preferred nomenclature for a two (2) in poker.</li>
 	<li>Common slang term for excrement</li>
</ol>
[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]