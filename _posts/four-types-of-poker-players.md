---
ID: 58
post_author: "1"
post_date: 2018-07-24 22:43:03
post_date_gmt: 2018-07-24 22:43:03
post_title: Four Types of Poker Players
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: four-types-of-poker-players
to_ping: ""
pinged: ""
post_modified: 2018-11-01 19:17:32
post_modified_gmt: 2018-11-01 19:17:32
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=58
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<p style="text-align: left;">It's important to mention that poker is more than just a gambling card game. It takes strategy, observation, dedication and, of course, a style. When playing poker, one of the most important aspects to consider is, obviously, your opponents. This means it's necessary to study their tendencies, psychology and emotions... their style of playing. This is only to determine which is the best way to approach them and win the pot. Learning how to identify your opponents is a must if you want to become one of the best. Why did they call or raise the bet? All these decisions are a statement that needs to be taking into consideration when betting or <a href="http://localhost/wordpress/texas-holdem-poker-glossary-bluff/">bluffing</a>. In this article we'll teach you the four types of poker players.</p>

<h2>Playing Styles</h2>
Usually, poker players can be classified by the style they play. There are two main factors that define their style: <strong>tight vs loose</strong>; and <strong>passive vs aggressive</strong>.
<h3>Tight VS Loose</h3>
<ul>
 	<li>Tight: a tight player is usually the player that only plays a<strong> small amount of deals</strong>, commonly the best ones.</li>
 	<li>Loose: a loose player is the player that <strong>plays in most deals</strong>.</li>
</ul>
Being one of them <strong>only refers to how they select the deals not how they play</strong>. The way they play (aggressive or passive) determines it; thus, the way you play against them.
<h3>Passive VS Aggressive</h3>
<ul>
 	<li>Passive: a passive player tends to <strong>play fearful of loosing and avoiding confrontations</strong>.</li>
 	<li>Aggressive: an aggressive player is the one that <strong>raises the bet and it's not afraid of losing or confrontation</strong>.</li>
</ul>
Now, once you've determined whether your opponent is thigh or loose; you must determine if he/she is a passive or aggressive player. This categorization will help you to plan your strategy more accurately.

<img class="wp-image-395 aligncenter" src="http://localhost/wordpress/wp-content/uploads/2018/07/bluff-1-300x200.png" alt="" width="629" height="420" />
<h2>The Four Types of Poker Players</h2>
The types of poker players are commonly grouped by the way they play. The four  types of poker players or combinations are: <strong>Rock</strong>, <strong>Calling Station</strong>, <strong>Shark</strong> and <strong>Maniac.</strong>

• <strong>Tight-passive</strong> also known as the “<strong>rock</strong>”
• <strong>Loose-passive</strong> also known as the “<strong>calling station</strong>”
• <strong>Tight-aggressive</strong> (<strong>TAG</strong>) also known as the “<strong>shark</strong>”
• <strong>Loose-aggressive</strong> (<strong>LAG</strong>) also known as the “<strong>maniac</strong>”

&nbsp;

[caption id="attachment_400" align="alignleft" width="300"]<img class="size-medium wp-image-400" src="http://localhost/wordpress//wp-content/uploads/2018/07/d0e4d72e10-300x199.jpg" alt="acttitude in poker" width="300" height="199" /> Danny Tang[/caption]
<h3>The Tight-Passive or Rock</h3>
The Tight-Passive or Rock type of poker player is very easy to read and identify. They generally call pre-flop only if have a good hand and won't play in many deals. When they actually have a good hand other players fold, since they can't bluff. They play in such a tight range of deals that this style is also referred as "weak-passive", which is why they're called "<strong>rock</strong>" or "<strong>nit</strong>". This player is so scare of losing that won't take any shot and are very easy to bluff.

&nbsp;

&nbsp;

&nbsp;

[caption id="attachment_467" align="alignleft" width="300"]<img class="size-medium wp-image-467" src="http://localhost/wordpress//wp-content/uploads/2018/07/Guy_Laliberte_2-300x200.jpg" alt="Guy Laliberte, the player poker" width="300" height="199" /> Guy Laliberte[/caption]
<h3>The Loose-Passive or Calling Station</h3>
The Loose-Passive or Calling Station type of players are very easy targets and obvious to stop. They usually call/raises <em>just to see a flop</em> and won't fold as long as they hit any card. This kind of player rarely becomes aggressive or take chances, tending to just be "<strong>calling stations</strong>". They let the other players take the risks. This player unlike the tight-passive isn't easy to bluff.

&nbsp;

&nbsp;

&nbsp;

[caption id="attachment_416" align="alignleft" width="300"]<img class="size-medium wp-image-416" src="http://localhost/wordpress//wp-content/uploads/2018/07/Johnny_Chan-300x200.jpg" alt="Johnny Chan a guy player poker" width="300" height="200" /> Johnny Chan[/caption]
<h3>The Tight-Aggressive or Shark</h3>
The Tight-Aggressive or Shark type of player won't play in many deals but when they do, they play strong. They wait for the best opportunities but aren't afraid of betting, that's why they're called shark". Their style is very effective, which is why most opponents, if observant, won't play against them.

&nbsp;

&nbsp;

&nbsp;

[caption id="attachment_419" align="alignleft" width="300"]<img class="size-medium wp-image-419" src="http://localhost/wordpress//wp-content/uploads/2018/07/Tomwan-300x200.jpg" alt="Tom Dwan, player poker" width="300" height="200" /> Tom Dwan[/caption]

&nbsp;
<h3>The Loose-Aggressive or Maniac</h3>
The Loose-Aggressive or Maniac  type of player will play in most deals, raising and re-raising in a wide variety of pre-flops and flops. They are very difficult to read since they play in such a variety of deals. They're not afraid of loosing and are bluffers by nature, which makes them a hard opponent. However, this "<strong>maniac</strong>" tendency to raise without a pattern means that you'll eventually win the pot against this type of player.

&nbsp;

&nbsp;
<h2>Tricky or Straightforward?</h2>
It's important to consider that not all poker players fit perfectly in one if above groups. Once you've categorized your opponents into those groups you need to determine if they're <strong>tricky</strong> or <strong>straightforward</strong>. It's a major factor to study, since not all maniacs are tricky, and not all sharks straightforward.  This category will help you to understand even more your opponents and will make your decisions more profitable and accurate.
<h2>Which is the Best Playing Style?</h2>
Poker is a game of aggressiveness. Remember, this is a gambling game, conflict is at its core. An aggressive approach and a solid strategy is critical to succeed. Most experts would say that a tight-aggressive style is the most profitable one. However, you should choose the style that fits your personality more. It's important to say that most successful poker players adapt their style to the conditions of the game.

https://www.youtube.com/watch?v=J1uU-JsIWIk