---
ID: 1624
post_author: "1"
post_date: 2018-09-07 13:40:43
post_date_gmt: 2018-09-07 13:40:43
post_title: 'Texas Holdem Poker Glossary: Bad Beat'
post_excerpt: >
  Bad Beat is when a player wins with a
  hand that looked weak. The loser player
  is said to have suffered from bad beat.
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-bad-beat
to_ping: ""
pinged: ""
post_modified: 2018-11-07 15:45:56
post_modified_gmt: 2018-11-07 15:45:56
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1624
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is Bad Beat?</h2>
Bad Beat is a term used in poker to express the unexpected win of a player that had what it seemed to be a weak hand. As for the player with the stronger and loser hand, well he/she suffered from a Bad Beat. It's important to mention, that most players can't agree whether a particular hand was a bad beat or not.

[adinserter name="Block 1"]

&nbsp;
<h3><img class=" wp-image-2313 aligncenter" src="http://localhost/wordpress/wp-content/uploads/2018/09/68fdad42-day3_sismix18-74_bd-300x200.jpg" alt="" width="621" height="453" /></h3>
&nbsp;
<h3>The Mother of All Poker Battles: Mike Matusow vs Phil Hellmuth - Video -</h3>
This could be one of the most exciting videos we've ever posted. We were thrilled by it. It's a video that catches our attention throughout the entire game. It's an excellent battle, where every poker strategy and resource are used and put out to the test. The attitude and reactions of each player really took our attention. The final is more exciting, thrilling, even. From minute 24, when Hellmuth rises up to celebrate a play, the video turns funny. But a minute after Matusow rises with the deal in his hands... winning the battle. It definitely is a very fascinating victory!

https://www.youtube.com/watch?v=vmnLNYCTfJY

&nbsp;
<h2>Types of Bad Beats</h2>
<ul>
 	<li><strong>When a weaker hand wins over a favorite hand:</strong> you can lose a deal with a pair of Aces to a 2/3. This is possible due to the community cards (<a href="http://localhost/wordpress/texas-holdem-poker-glossary-turn/">turn</a> and <a href="http://localhost/wordpress/texas-holdem-poker-glossary-river/">river</a>) in Texas Holdem Poker. When it happens players might not control their frustration and some won't recover from the lost. Phill Hellmuth is very famous for losing his temper when suffering from Bad Beats.</li>
 	<li><strong>A very strong hand loses to a even stronger hand: </strong>losing a hand with a <a href="http://localhost/wordpress/best-texas-holdem-poker-hands/">full house</a> to a <a href="http://localhost/wordpress/best-texas-holdem-poker-hands/">straight flush</a> is not only a cinematography fantasy. In real life a good-strong hand can lose against an even better thanks to the community cards.</li>
</ul>
<img class=" wp-image-2110" src="http://localhost/wordpress/wp-content/uploads/2018/09/9c566b58-juzmajgoe61up9wizzlm-300x169.jpg" alt="" width="602" height="339" />

Billy Pappas

&nbsp;
<h2>Bad Beat Jackpot</h2>
A Bad Beat Jackpot is a prize that casinos pay only when you've lost against a stronger hand. Not all Texas Holdem Poker games have a Bad Beat Jackpot. And those who offer it, have requirements on how strong the hand has to be. Most Texas Holdem Poker Jackpots require a <a href="http://localhost/wordpress/best-texas-holdem-poker-hands/">four-of-a-kind</a> hand as minimum, and the use of both Hole Cards.

If someone manages to win a Jackpot, it's usually split in the following way: 50% of the pot goes to the winner of the jackpot, 25% to the loser of the jackpot and 25% to the rest of the players of the hand.
<h3 class="title style-scope ytd-video-primary-info-renderer">Phil Hellmuth Worst Beat Ever - The Big Game Season 1 - Video -</h3>
The next video shows a very hard play to Phill Hellmuth. In this video we can see the participation of Daniel Negreanu, Ernest Wiggins, Tony G and Doyle Brunson. They're all high stakes players and famous worldwide poker players who've won many tournaments. In this deal are Hellmuth and Wiggins who steel the show. Helmuth has a 3 of a kind and Wiggins a pair of Kings. They decide to running it four times and the result and the rest of the contestants reactions upsets Hellmuth.

&nbsp;

https://www.youtube.com/watch?v=XCL9_2gRZI0

[adinserter name="Block 2"]
<h3 class="title style-scope ytd-video-primary-info-renderer">Phil Hellmuth Gets an Insane Bad Beat - Video -</h3>
The minute is 3:23. That is the moment when it becomes evident to Hellmuth that he is going to lose. While Ferguson is a very passive player, showing no emotion -even when it's clear that he has won- Hellmuth is starting to lose his temper. This steadiness makes more evident Hellmuth's anger for losing. Now, throughout the deal we can appreciate Hellmuth standing up and even lying down at the end just to express his ire. This is a very surprising bad beat.

&nbsp;

https://www.youtube.com/watch?v=A_XIH5M0BOE

[adinserter name="Block 3"]