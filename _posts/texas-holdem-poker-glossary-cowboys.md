---
ID: 1663
post_author: "8"
post_date: 2018-09-07 14:14:11
post_date_gmt: 2018-09-07 14:14:11
post_title: 'Texas Holdem Poker Glossary: Cowboys'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-cowboys
to_ping: ""
pinged: ""
post_modified: 2018-10-05 15:43:03
post_modified_gmt: 2018-10-05 15:43:03
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1663
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2 class="glossarySection--content-headline">Cowboys</h2>
Dos reyes en una mano inicial de <i><a class="glosslink" href="https://es.pokerstrategy.com/glossary/Texas-Holdem/">Texas Hold'em</a></i> son conocidos como los <i><a class="glosslink" href="https://es.pokerstrategy.com/glossary/Cowboys/">cowboys</a></i> (vaqueros en español).
<div id="tw-target-text-container" class="gsrt tw-ta-container tw-nfl">
<pre id="tw-target-text" class="tw-data-text tw-ta tw-text-small" dir="ltr" data-placeholder="Traducción" data-fulltext=""><span lang="en">Two kings in an initial hand of Texas Hold'em are known as cowboys (cowboys in Spanish).

</span></pre>
<pre class="tw-data-text tw-ta tw-text-small" lang="en"></pre>
</div>
[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]