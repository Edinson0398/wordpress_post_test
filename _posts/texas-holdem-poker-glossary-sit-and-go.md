---
ID: 1767
post_author: "1"
post_date: 2018-09-07 15:17:09
post_date_gmt: 2018-09-07 15:17:09
post_title: 'Texas Holdem Poker Glossary: Sit-And-Go'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-sit-and-go
to_ping: ""
pinged: ""
post_modified: 2018-11-07 18:31:48
post_modified_gmt: 2018-11-07 18:31:48
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1767
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h3>SIT AND GO</h3>
https://es-betfair.custhelp.com/app/answers/detail/a_id/5762/~/¿qué-son-los-torneos-sit-and-go%3F

A <a class="definitionMatch" href="https://en.clubpoker.net/sit-and-go/definition-472" data-ipstooltip="">sit and go</a> is a single-<a class="definitionMatch" title="Table definition" href="https://en.clubpoker.net/table/definition-536" data-ipstooltip="">table</a> <a class="definitionMatch" title="Freezeout definition" href="https://en.clubpoker.net/freezeout/definition-219" data-ipstooltip="">freezeout</a> <a class="definitionMatch" title="Tournament definition" href="https://en.clubpoker.net/tournament/definition-579" data-ipstooltip="">tournament</a> that begins as soon as all of the available seats <a class="definitionMatch" title="Fill Up definition" href="https://en.clubpoker.net/fill-up/definition-188" data-ipstooltip="">fill up</a>. The original SNGs began as ten-handed single table tournaments, also known as STTs. When the tenth player signed up, the tournament commenced. Over the years, SNGs have evolved to include multi-table tournaments with a set number of entrants, like 30-player or 180-player SNGs.

&nbsp;

&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]
<p style="padding-left: 30px;">[adinserter name="Block 13"]</p>
[adinserter name="Block 14"]

[adinserter name="Block 15"]