---
ID: 1631
post_author: "1"
post_date: 2018-09-07 13:42:54
post_date_gmt: 2018-09-07 13:42:54
post_title: 'Texas Holdem Poker Glossary: Blank'
post_excerpt: "Blank a card, that is part of the community cards, that doesn't increase nor decrease your chances to get a good hand and win the pot."
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-blank
to_ping: ""
pinged: ""
post_modified: 2018-11-07 17:25:31
post_modified_gmt: 2018-11-07 17:25:31
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1631
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is a Blank?</h2>
A Blank, in Texas Holdem Poker, is a card that doesn't increase nor decrease your chances to win the pot. This card is usually from the <a href="http://localhost/wordpress/best-texas-holdem-poker-hands/">community cards</a>.

It is normally represented by lower ranked cards, such as 2's or 3's. However, it doesn't necessarily has to be a low ranked card. It can be a high ranked card that simply doesn't favor either player.

It's important to mention that another way to refer to blank cards is <a href="http://localhost/wordpress/texas-holdem-poker-glossary-rag-rags/">rag</a>. You can use both, since they have similar -more like identical- meanings.

Ragged is a flop or board that doesn't contribute to any of the players. A worthless card round or board to any or all players in the deal.

As an illustration, let's imagine that you have an Ace and a Joker, and your opponent a pair of Kings. The flops comes Queens-10s-Kings so you bet and your opponent calls. Now, the turn gives a Joker, you bet and your opponent calls. As for the river, it brings 5s.

The last card or river didn't helped any of you. So, it's a blank card, a card that didn't contribute to the final hands. The final hands are two pairs and three of a kind.
<h3>Best Starting Hands - Poker Tutorials - Video -</h3>
To really understand things, sometimes, we need to study about everything that surrounds it. Even if it its the opposite! And that's our case. In this video we're going to learn about the best starting hands in poker. Nicky Numbers teaches us what are the best possible hands during the pre-flop round. This information is very helpful, since he also tells us what other card are useful with those combinations. For us this is a great thing, given the fact that we need to identify those blank cards!

https://www.youtube.com/watch?v=aN6_BPBy4tA

&nbsp;
<h3 class="title style-scope ytd-video-primary-info-renderer">Top 10 Most Amazing Poker Hands Ever!</h3>
<div id="info" class="style-scope ytd-video-primary-info-renderer">The 10 most amazing poker hands you've seen! It's always a good idea to learn from the best! So, we're giving you some of the best hands for you to learn how to play like a professional!</div>
<div></div>
https://www.youtube.com/watch?v=oBoLdXLx9So

&nbsp;

&nbsp;

&nbsp;

<img class="wp-image-2437 aligncenter" src="http://localhost/wordpress/wp-content/uploads/2018/09/abb1b695-hand-selection-bluffing-300x169.jpg" alt="" width="923" height="520" />

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]

[adinserter name="Block 15"]