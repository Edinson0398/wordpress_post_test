---
ID: 1766
post_author: "1"
post_date: 2018-09-07 15:16:42
post_date_gmt: 2018-09-07 15:16:42
post_title: 'Texas Holdem Poker Glossary: Side Pot'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-side
to_ping: ""
pinged: ""
post_modified: 2018-11-07 17:20:06
post_modified_gmt: 2018-11-07 17:20:06
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1766
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
[adinserter name="Block 1"]
<h2>Side Pot</h2>
Created once a player is all in and other players continue betting "on the side" (literally). The side pot is distinct from the main pot in that only the players still betting play for it, while the all-in player can only win the main pot. If there are multiple all-in players, multiple side pots can potentially be created.

For example, in a $1/$2 no-limit hold'em game, Player A raises to $8 from early position, then Player B reraises all in with his last $20 from the cutoff seat. Player C then calls the reraise from the button, the blinds fold, and Player A calls as well. The main pot having been created, Players A and C will then bet into a side pot after the flop if they wish.

[adinserter name="Block 2"]

&nbsp;
<h3 class="title style-scope ytd-video-primary-info-renderer">Poker Side Pot - Video -</h3>
A quick and simple tutorial on how to make a side pot. It's a good explication because we can to see a table with all. The explanation is from the table, in a senital way, you can easily see the table with all the cards and cards, and the girl <span lang="en">explains calmly what we should do to achieve a Side pot. </span>
<div id="info" class="style-scope ytd-video-primary-info-renderer"></div>
https://www.youtube.com/watch?v=rxXzo3UXitY

[adinserter name="Block 3"]

&nbsp;
<h3 class="title style-scope ytd-video-primary-info-renderer">Side Pots - Video -</h3>
This video it's a short video about the explication of Main pot and Side pots; you can to see a text with a the significant. It's simple, but it's possible that this video can help you and serve.

https://www.youtube.com/watch?v=4NcxvgNKyVI

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]