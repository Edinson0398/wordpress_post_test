---
ID: 1787
post_author: "1"
post_date: 2018-09-07 15:20:51
post_date_gmt: 2018-09-07 15:20:51
post_title: 'Texas Holdem Poker Glossary: Turn'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-turn
to_ping: ""
pinged: ""
post_modified: 2018-11-07 15:36:03
post_modified_gmt: 2018-11-07 15:36:03
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1787
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
In Texas Holdem Poker there are four <a href="http://localhost/wordpress/texas-holdem-poker-betting-structure/">betting rounds</a>. That said, the third betting round is known as <strong>turn</strong> or <strong>fourth street</strong>.
<h2>What is Turn?</h2>
Turn, in Texas Holdem Poker, is the fourth card of the community cards. It is also the third time you have the chance to check (if possible) bet, call or raise. As in the previous rounds (pre-flop and flop) the first player to have action is the player next to the immediate left of dealer.

All rounds end after all bets are equalized or checked by all players. It's important to mention that if a player raises or re-reases the bet and no other player calls he/she wins the deal without having to show his/her hand.

<img class="alignnone wp-image-2230" src="http://localhost/wordpress/wp-content/uploads/2018/09/6d324afc-13-levels-of-losing-at-the-poker-table-1050x600-300x171.jpg" alt="" width="673" height="384" />

[adinserter name="Block 1"]

[adinserter name="Block 2"]
<h3></h3>
<h3>Fatal Turn in Poker - Straight of Mike Sexton come across on flush -Video -</h3>
This seven minute video shows us an exceptional duel between two players. In one hand, we have Mizzi with a strong hand made up of an Ace and a King suited. In the other hand, we have Sexton with a good hand, a hand that can improve, made of a suited ten and an eight. From the beginning to the end of the hand the spot light is on two players, their attitude and behavior. Yet, the game is not defined until the turn round. This is the moment when it become evident (for us viewers) that Mizzi is the crowned winner. Another important highlight of the video is that since Sexton folded, Mizzi didn't have to show his cards.

&nbsp;

https://www.youtube.com/watch?v=uQ6jo0fP8tg
<h3></h3>
<h3>Top 5 Best Poker Turn of events Hands! - Video -</h3>
This video gives us 5 great plays in which the turn round has a very important function and role in the definition of all winning hands. It help us to understand more the importance of the turn round and give us tips on how to play a good hand.

https://www.youtube.com/watch?v=wP3or8S_UMw

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]