---
ID: 51
post_author: "1"
post_date: 2018-12-24 22:26:34
post_date_gmt: 2018-12-24 22:26:34
post_title: >
  The Rule of 4 and 2 in Texas Holdem
  Poker
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: >
  the-rule-of-4-and-2-in-texas-holdem-poker
to_ping: ""
pinged: ""
post_modified: 2018-09-10 21:16:47
post_modified_gmt: 2018-09-10 21:16:47
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=51
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>What is The Rule of 4 and 2?</h2>
The Rule of 4 and 2, or The 2/4 Rule, was conceived by Phil Gordon. It was published in his no-limit strategy book Little Green Book: Lessons and Teachings in No Limit Texas Hold'em. It's a quick system to calculate the odds of winning the pot

[adinserter name="Block 1"]
<h2>How to calculate your percentage to hit</h2>
<span style="color: #ff0000;">• Multiply your outs by 2 when you are on the flop wating for the turn card.</span>
<span style="color: #ff0000;">• Multiply your outs by 2 when you are on the turn waiting for the river card.</span>
<span style="color: #ff0000;">• Multiply your outs by 4 when you are on the flop waiting for the river card (opponent is all-in).</span>

<span style="color: #ff0000;">When you have multiplied your outs by either 4 or 2, you will get a percentage that you can compare with your pot odds to work out whether or not is worth calling with a drawing hand.</span>

[adinserter name="Block 2"]

<span style="color: #ff0000;">An out is an unseen card, that if drawn, will improve a player’s hand to one that is likely to win. Knowing the number of outs a player has is an important part of poker strategy.</span>

<span style="color: #ff0000;">For example:</span>

<span style="color: #ff0000;">You have a flush draw on the flop in a $0.50/$1 NL Hold’em game. Your opponent bets $10 in to the $10</span>
<span style="color: #ff0000;">pot. The pot is now $20 and you have to call $10 to continue to try and hit your flush. Should you call or</span>
<span style="color: #ff0000;">fold?</span>
<span style="color: #ff0000;">Hand:A 2</span>
<span style="color: #ff0000;">Board: J 3 7</span>
<span style="color: #ff0000;">Final</span>
<span style="color: #ff0000;">Pot:$20</span>
<span style="color: #ff0000;">To Call: $10</span>

[adinserter name="Block 3"]

<span style="color: #ff0000;">With a flush draw we have 9 outs, and seeing as we are on the flop waiting for the turn, we will use the</span>
<span style="color: #ff0000;">"rule of 2".</span>
<span style="color: #ff0000;">• Odds of completing our flush: 9 outs * 2 = 18%</span>
<span style="color: #ff0000;">• Pot odds: 33% ($30 pot total including our call. $10 is 33% of $30.)</span>
<span style="color: #ff0000;">The odds of completing our flush are worse than the odds we are getting from the pot, therefore we</span>
<span style="color: #ff0000;">should fold. To put it another way, we do not want to call more than 18% of the pot size to continue, so</span>
<span style="color: #ff0000;">we fold.</span>

[adinserter name="Block 5"]

<span style="color: #ff0000;">Rule of 4 accuracy table:</span>
<span style="color: #ff0000;">Outs 4 and 2 Actual Difference</span>
<span style="color: #ff0000;">4 (gutshot) 16% 16.5% -0.5%</span>
<span style="color: #ff0000;">8 (straight) 32% 31.5% +0.5%</span>
<span style="color: #ff0000;">9 (flush) 36% 35% +1%</span>
<span style="color: #ff0000;">15 (straight +</span>
<span style="color: #ff0000;">flush) 60% 54.1% +5.9%</span>
<span style="color: #ff0000;">The percentage odds are impressively close when using the rule of 4 (i.e. when your opponent is all-in on</span>
<span style="color: #ff0000;">the flop and there are two cards to come).</span>
<span style="color: #ff0000;">The only draw that's noticeably off is the straight + flush draw when you have 15 outs (5.9% difference),</span>
<span style="color: #ff0000;">but that doesn't really matter because when you've got a greater than 50% chance of winning the pot.</span>
<span style="color: #ff0000;">Rule of 2 accuracy table.</span>
<span style="color: #ff0000;">The rule of 2 can be used on both the flop and the turn. So to keep things simple, in the following table I'll</span>
<span style="color: #ff0000;">just compare the percentage odds for when we are on the turn waiting for the river.</span>
<span style="color: #ff0000;">Outs 4 and 2 Actual Difference</span>
<span style="color: #ff0000;">4 (gutshot) 8% 8.7% -0.7%</span>
<span style="color: #ff0000;">8 (straight) 16% 17.4% -1.4%</span>
<span style="color: #ff0000;">9 (flush) 18% 19.6% -1.6%</span>
<span style="color: #ff0000;">15 (straight +</span>
<span style="color: #ff0000;">flush) 30% 32.6% -2.6%</span>

[adinserter name="Block 6"]

&nbsp;