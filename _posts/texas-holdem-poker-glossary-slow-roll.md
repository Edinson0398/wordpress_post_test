---
ID: 1773
post_author: "1"
post_date: 2018-09-07 15:17:49
post_date_gmt: 2018-09-07 15:17:49
post_title: 'Texas Holdem Poker Glossary: Slow Roll'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-slow-roll
to_ping: ""
pinged: ""
post_modified: 2018-11-07 16:01:27
post_modified_gmt: 2018-11-07 16:01:27
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1773
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h3>SLOW ROLL</h3>
In <a class="definitionMatch" href="https://en.clubpoker.net/partypoker-live-partypoker/or-137,com" data-ipstooltip="">live</a> games, you are supposed to reveal your cards in a timely matter. A <a class="definitionMatch" title="Slowroll definition" href="https://en.clubpoker.net/slowroll/definition-477" data-ipstooltip="">slowroll</a> occurs when you delay tabling your cards until your opponent shows what they think is a winning hand, then you arrogantly display the winning hand. Slowrolling is poor poker etiquette and one of the most disrespectful acts you can do at the tables.
<div class="pagination">
<div class="paginationBack">Acción poco decorosa que consiste en demorarse en enseñar las cartas, o en hacer Call, cuando se tiene una jugada mejor que el adversario, haciéndole así creer que ha ganado la mano.</div>
</div>
La revelación lenta y progresiva de cartas de una en una en el <i><a class="glosslink" href="https://es.pokerstrategy.com/glossary/Showdown/">showdown</a></i>. Normalmente solo se hace con una mano ganadora para molestar o enfadar a un rival que aún piensa que lleva la mejor mano.

&nbsp;

&nbsp;

&nbsp;

[adinserter name="Block 1"]

&nbsp;
<h3>Top 5 Best Poker Slow rolls of all time! - Video -</h3>
&nbsp;

https://www.youtube.com/watch?v=7ybnK_l6AXc

&nbsp;

&nbsp;

&nbsp;

[adinserter name="Block 2"]
<h3>Top 5 Best Poker Slow rolls of all time! - Video -</h3>
&nbsp;

https://www.youtube.com/watch?v=K9Am0D87tEc

&nbsp;

&nbsp;

[adinserter name="Block 3"]
<h3>Top 5 Best Poker Slow rolls of all time! - Video -</h3>
&nbsp;

https://www.youtube.com/watch?v=-nidPzeJ3fg

&nbsp;

[adinserter name="Block 4"]
<h3>Top 5 Best Poker Slow rolls of all time! - Video -</h3>
https://www.youtube.com/watch?v=c8DoJvZ_810

&nbsp;

&nbsp;

&nbsp;

[adinserter name="Block 5"]

[adinserter name="Block 6"]
<p style="padding-left: 30px;">[adinserter name="Block 13"]</p>
[adinserter name="Block 14"]

[adinserter name="Block 15"]