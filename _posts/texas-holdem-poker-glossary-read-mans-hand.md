---
ID: 1666
post_author: "1"
post_date: 2018-09-07 14:14:30
post_date_gmt: 2018-09-07 14:14:30
post_title: "Texas Holdem Poker Glossary: Read Man's Hand"
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: >
  texas-holdem-poker-glossary-read-mans-hand
to_ping: ""
pinged: ""
post_modified: 2018-11-12 16:02:18
post_modified_gmt: 2018-11-12 16:02:18
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1666
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>Read Man's Hand</h2>
The dead <b>man's hand</b> is a nickname for a particular <b>poker hand</b>, popularly a two-pair of black aces and black eights, although definitions of the <b>hand</b> have varied through the years. Such a <b>hand</b> is said to have been held by Old West folk hero, lawman, and gunfighter Wild Bill Hickok when he was murdered.

&nbsp;

[adinserter name="Block 1"]

[adinserter name="Block 2"]

[adinserter name="Block 3"]

[adinserter name="Block 4"]

[adinserter name="Block 5"]

[adinserter name="Block 6"]

[adinserter name="Block 13"]

[adinserter name="Block 14"]

[adinserter name="Block 15"]