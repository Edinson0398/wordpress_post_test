---
ID: 1668
post_author: "1"
post_date: 2018-09-07 14:14:43
post_date_gmt: 2018-09-07 14:14:43
post_title: 'Texas Holdem Poker Glossary: Dealer'
post_excerpt: ""
post_status: private
comment_status: open
ping_status: open
post_password: ""
post_name: texas-holdem-poker-glossary-dealer
to_ping: ""
pinged: ""
post_modified: 2018-11-03 02:25:16
post_modified_gmt: 2018-11-03 02:25:16
post_content_filtered: ""
post_parent: 0
guid: http://localhost/wordpress/?p=1668
menu_order: 0
post_type: post
post_mime_type: ""
comment_count: "0"
filter: raw
---
<h2>Dealer</h2>
Jugador desde cuya posición se reparten las cartas.

Aquél que reparte las cartas. No tiene por qué ser el que reparta realmente las cartas, pero es el que debería hacerlo según las reglas; el orden de las acciones se decide como si estuviera repartiendo. De ahí que, en <i>Texas Hold'em</i>, cada jugador es el <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Repartidor/">repartidor</a> un turno por ronda, aunque el que realmente reparta las cartas sea un trabajador del casino o un programa de software. El jugador asignado como <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Repartidor/">repartidor</a> cambia cada mano en el sentido de las agujas del reloj. El <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Repartidor/">repartidor</a> en cada mano se identifica por una pequeña ficha, llamada la ficha del repartido (<i>dealer button</i> en inglés), que también se mueve alrededor de la <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Mesa/">mesa</a> conforme lo hace el <a class="glosslink" href="https://es.pokerstrategy.com/glossary/Repartidor/">repartidor</a>.

&nbsp;
<h3>How to Deal Poker . How to shuffle cards - Train with me - Video -</h3>
Este chico nos muestra la manera en que se desenvuelve un dealer profesional del poker, con sumo detenimiento, nos muestra poco a poco, pasos, formas de barajar las cartas, cómo repartirlas y diversos puntos importantes para cualqueir dealer al momento de manejarse en un partido. No te lo pierdas!

https://www.youtube.com/watch?v=pVyu6VWTfoU

[adinserter name="Block 1"]

&nbsp;
<h3>Dealer Olympics 2005 World Series of Poker Part 5 - Video -</h3>
Este es un video muy entretenido, donde vemos diferentes Dealers practicando sus habilidades y sobre todo la rapidez que manejan para repartir las cartas y desarrollar su gestión en la mesa, realmente es una competencia de Olimpiadas del 2005, si ingresas a youtube a través de este link, podrás conseguir mayor diversidad de Dealer y ejemplos, la idea en este concurso es que los Dealer logren ordenar las cartas según la pinta, en el menor tiempo posible.  Podrás notar la dificultad que esto representa para algunos y la habilidad realmente eficiente de muchos otros.

https://www.youtube.com/watch?v=srZ15dSskPw

&nbsp;

[adinserter name="Block 2"]

[adinserter name="Block 3"]

&nbsp;